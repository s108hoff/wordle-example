export function testGuess(word, guess) {
    let output = []
    if (word === guess) {
        return true
    }

    for (let charIndex = 0; charIndex < guess.length; charIndex++) {
        const guessChar = guess[charIndex]
        const wordChar = word[charIndex]

        if (guessChar === wordChar) {
            output.push({
                char: guessChar,
                isCorrect: true
            })
        } else {
            output.push({
                char: guessChar,
                isCorrect: false
            })
        }
    }

    return output
}
