import { runGame } from "./game-html.js"

const words = [
    'bet',
    'net',
    'runner',
    'coding',
]

let word = words[Math.floor(Math.random() * words.length)]
let maxGuesses = word.length

const game = runGame(word, maxGuesses)

document.getElementById('reset-button').addEventListener('click', () => {
    word = words[Math.floor(Math.random() * words.length)]
    maxGuesses = word.length

    game.newWord(word, maxGuesses)
})

