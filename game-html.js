import { testGuess } from "./word.js"

function createGuessElements(maxGuesses) {
    const guessElements = []

    for (let i = 0; i < maxGuesses; i++) {
        const guessElement = document.createElement('li')
        guessElement.id = 'guess' + i
        guessElements.push(guessElement)
    }

    return guessElements
}

export function runGame(word, maxGuesses) {
    const guessForm = document.getElementById('guess-form')
    const guessInput = document.getElementById('guess-input')
    const guessSubmitButton = document.getElementById('guess-submit-button')
    const wordCheatElement = document.getElementById('word-cheat')
    const wordLengthElement = document.getElementById('word-length')
    const resultElement = document.getElementById('result')
    const guessesElement = document.getElementById('guesses')
    let guessElements = createGuessElements(maxGuesses)

    let currentGuesses = 0

    guessesElement.append(...guessElements)
    guessInput.setAttribute('maxLength', word.length)
    guessInput.setAttribute('minLength', word.length)
    wordCheatElement.textContent = word
    wordLengthElement.textContent = `The word is ${word.length} characters long`

    guessForm.addEventListener('submit', (event) => {
        event.preventDefault()

        const guess = guessInput.value
        const results = testGuess(word, guess)
        let attemptOutput = guess + ': '

        if (results === true) {
            resultElement.textContent = 'You Won!'
            attemptOutput += guess
            guessInput.setAttribute('disabled', true)
            guessSubmitButton.setAttribute('disabled', true)
        } else {
            for (let result of results) {
                if (result.isCorrect) {
                    attemptOutput += result.char
                } else {
                    attemptOutput += '_'
                }
            }
        }

        guessElements[currentGuesses].textContent = attemptOutput
        guessInput.value = ''
        currentGuesses++

        if (currentGuesses >= maxGuesses) {
            resultElement.textContent = 'You lost'
            guessInput.setAttribute('disabled', true)
            guessSubmitButton.setAttribute('disabled', true)
        }
    })

    return {
        newWord(newWord, newMaxGuesses) {
            resultElement.textContent = ''

            guessElements = createGuessElements(maxGuesses)
            guessesElement.replaceChildren(guessElements)
        
            guessInput.setAttribute('disabled', false)
            guessSubmitButton.setAttribute('disabled', true)

            word = newWord
            maxGuesses = newMaxGuesses
        }
    }
}
