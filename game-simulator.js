import { testGuess } from "./word.js"

export function runGame(word, guesses, maxGuesses) {
    let currentGuesses = 0
    let success = false

    while (currentGuesses < maxGuesses) {
        const guess = guesses[currentGuesses]
        const results = testGuess(guess, word)
        if (results === true) {
            success = true
            showResults(guess, results)
            break
        }

        showResults(guess, result)

        currentGuesses++
    }

    if (success) {
        console.log('You won! The word was ' + word)
    } else {
        console.log('You lost, the word was ' + word)
    }
}

function showResults(guess, results) {
    let output = guess + ': '

    if (results === true) {
        output += guess
    } else {
        for (let result of results) {
            if (result.isCorrect) {
                output += result.char
            } else {
                output += '_'
            }
        }
    }
}
